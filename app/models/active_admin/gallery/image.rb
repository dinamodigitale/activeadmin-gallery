module ActiveAdmin::Gallery
  class Image < ActiveRecord::Base
    self.table_name = "active_admin_gallery_images"
    belongs_to :imageable, polymorphic: true

    image_accessor :image
    attr_accessible :image
    validates :image, presence: true, unless: :remove_image
    validates_size_of :image, :maximum => 3.megabyte, on: :create
    validates_property :format, of: :image, in: [:jpeg, :jpg, :png, :tif, :tiff, :gif], case_sensitive: false

    translates :title, :link, :link_text
    attr_accessible :translations
    attr_accessible :translations_attributes
    attr_accessible :retained_image, :alt, :position
    accepts_nested_attributes_for :translations

    def translations=(val)
      if val.is_a?(Array)
        translations_attributes = val
      else
        if val["id"].present?
          self.translations.find(val["id"]).update(val)
        else
          self.translations.new(val)
        end
      end
    end

    default_scope { order(:position) if column_names.include?('position') }
  end
end
